var gulp = require('gulp'),
    compass = require('gulp-compass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify-es').default,
    coffee = require('gulp-coffee'),
    watch = require('gulp-watch'),
    rename = require('gulp-rename'),
    include = require('gulp-include'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    theme = "wp-content/themes/solar-matrix/assets",
    path = {
      css: theme + "/css",
      js: theme + "/javascript",
      scss: theme + "/scss",
      images: theme + "/images",
      fonts: theme + "/fonts",
      coffee: theme + "/coffee",
    };

// Error
function onError(err) {
  console.log(err);
  this.emit('end');
}

gulp.task('build', ['coffee', 'styles', 'scripts']);
gulp.task('default', ['coffee', 'styles', 'scripts', 'watch']);


// Coffee
gulp.task('coffee', function() {
  gulp.src(path.coffee + '/**/*.coffee')
    .pipe(coffee({bare: true}))
    .on('error', onError)
    .pipe(gulp.dest(path.js));
});

gulp.task('styles', function() {
  gulp.src(path.scss + '/**/*.scss')
    .pipe(compass({
      require: ['sass-globbing', 'compass/import-once/activate'],
      sourcemap: true,
      css: path.css,
      font: path.fonts,
      sass: path.scss,
      image: path.images,
      style: "nested",
      comments: false,
      relative: true
    }))
    .on('error', onError)
    .pipe(cleanCSS({ compatibility: 'ie8' }))
    .on('error', onError)
    .pipe(autoprefixer({browsers: ['last 10 versions'], cascade: false }))
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest(path.css));
})

gulp.task('scripts', function() {
  return gulp.src([path.js + '/**/*.js', '!' + path.js + '/**/*.min.js'])
    .pipe(include())
    .pipe(uglify())
    .on('error', onError)
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest(path.js));
});

gulp.task('watch', function() {
  gulp.watch(path.coffee + '/**/*.coffee', ['coffee'])
  gulp.watch(path.scss + '/**/*.scss', ['styles'])
  gulp.watch([path.js + '/**/*.js', '!' + path.js + '/**/*.min.js'], ['scripts'])
});
